import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SignupComponent } from './signup/signup.component';
import { HomeComponent } from './home/home.component';
import { NewsComponent } from './news/news.component';

const routes: Routes = [
  { path: '', loadChildren: 'app/home/home.module', pathMatch: 'full' },
  { path: 'signup',  loadChildren: 'app/signup/signup.module' },
  { path: 'news',  loadChildren: 'app/news/news.module' },
  { path: 'calculator',  loadChildren: 'app/calculator/calculator.module' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
