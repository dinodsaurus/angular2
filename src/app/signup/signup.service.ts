import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Signup } from './signup';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class SignupService {
    firstStepSuccessFull = false;
  constructor(private http: Http) {}

  signup(user: Signup) {
    this.firstStepSuccessFull = false;
    return this.http
        .post(`app/signup`, user)
        .toPromise()
        .then(response => response.json() as Signup)
        .catch(err => {
            console.log(err);
            this.firstStepSuccessFull = true
        });
  }
}
