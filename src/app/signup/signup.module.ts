import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { SignupComponent } from './signup.component';
import { SignupService } from './signup.service';

const routes = [
  { path: '', component: SignupComponent}
];


@NgModule({
  declarations: [
    SignupComponent
  ],
  exports: [
    SignupComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    SignupService
  ]
})
export default class SignupModule {}
