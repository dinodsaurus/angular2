import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Signup } from './signup';
import { SignupService } from './signup.service';
@Component({
  selector: 'signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {
  firstStep = this.signupService.firstStepSuccessFull;
  constructor(private signupService: SignupService){}
  onSubmit(f: NgForm) {
    const signupForm: Signup = f.value;
    console.log(signupForm.email);
    console.log(f.valid);
    if (f.valid) {
      console.log('prior');
      console.log(this.signupService.firstStepSuccessFull)
    }
  }
}
