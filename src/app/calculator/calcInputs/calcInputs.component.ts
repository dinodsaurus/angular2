import { Component } from '@angular/core';
import { CalculatorService } from '../calculator.service';

@Component({
  selector: 'calculator-inputs',
  templateUrl: './calcInputs.component.html',
  styleUrls: [ './calcInputs.component.css' ]
})

export class CalculatorInputs {
    private firstNumber: number;
    private secondNumber: number;
    constructor(private calculatorService: CalculatorService) {}
    calculateNumbers() {
      const firstNumber = this.firstNumber;
      const secondNumber = this.secondNumber;
      this.calculatorService.calculateNumbers(firstNumber, secondNumber);
    }
}
