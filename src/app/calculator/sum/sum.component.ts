import { Component, Input } from '@angular/core';

@Component({
  selector: 'calculator-sum',
  templateUrl: './sum.component.html'
})

export class SumComponent {
    @Input() sum: number;
    constructor() {}
}
