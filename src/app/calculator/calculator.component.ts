import { Component } from '@angular/core';
import { CalculatorService } from './calculator.service';

@Component({
  selector: 'calculator',
  templateUrl: './calculator.component.html'
})

export class CalculatorComponent {
    private result = {
      sum: 0,
      multi: 0
    };
    constructor(private calculatorService: CalculatorService) {
      calculatorService.calcResult$.subscribe((calcResult: {
        sum: number,
        multi: number
      }) => {
        this.result = calcResult;
      })
    }
}
