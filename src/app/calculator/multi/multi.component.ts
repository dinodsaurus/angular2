import { Component, Input } from '@angular/core';

@Component({
  selector: 'calculator-multi',
  templateUrl: './multi.component.html'
})

export class MultiComponent {
    @Input() multiply: number;
    constructor() {}
}
