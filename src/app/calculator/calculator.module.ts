import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { CalculatorComponent } from './calculator.component';
import { CalculatorInputs } from './calcInputs/calcInputs.component';
import { SumComponent } from './sum/sum.component';
import { MultiComponent } from './multi/multi.component';

import { CalculatorService } from './calculator.service';

const routes = [
  { path: '', component: CalculatorComponent}
];

@NgModule({
  declarations: [
    CalculatorComponent,
    CalculatorInputs,
    SumComponent,
    MultiComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  providers: [ CalculatorService ]
})
export default class CalculatorModule {}
