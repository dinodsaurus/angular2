import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class CalculatorService {
    private calcResult = new Subject();
    public readonly calcResult$ = this.calcResult.asObservable();
    constructor() {}

    calculateNumbers(firstNumnber: number, secondNumber: number) {
        const sumResult = this.sumNumbers(firstNumnber, secondNumber);
        const multiResult = this.multiplyNumbers(firstNumnber, secondNumber);

        // Push the new values to subscribers
        this.calcResult.next({
            sum: sumResult,
            multi: multiResult
        });
    }

    multiplyNumbers(firstNumnber: number, secondNumber: number) {
        return firstNumnber * secondNumber;
    }
    sumNumbers(firstNumnber: number, secondNumber: number) {
        return firstNumnber + secondNumber;
    }
}

