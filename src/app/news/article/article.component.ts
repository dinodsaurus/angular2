import { Component, Input } from '@angular/core';
import { Article } from '../news';

@Component({
  selector: 'news-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent {
    @Input() inArticle: Article;
    goTo(e, article) {
      e.preventDefault();
      window.location.href = article.url;
    }
}
