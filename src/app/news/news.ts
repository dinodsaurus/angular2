export class Article {
  descendants: number;
  by: string;
  id: number;
  kids: number[];
  score: number;
  time: number;
  title2: string;
  url2: number;
}
