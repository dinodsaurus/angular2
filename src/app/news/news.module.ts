import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NewsComponent } from './news.component';
import { NewsDetailsComponent } from './details/details.component';
import { ArticleComponent } from './article/article.component';
import { NewsService } from './news.service'

const routes = [
  { path: '', component: NewsComponent},
  { path: ':id', component: NewsDetailsComponent}
];

@NgModule({
  declarations: [
    NewsComponent,
    ArticleComponent,
    NewsDetailsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  providers: [ NewsService ],
  exports: [
    NewsComponent
  ]
})
export default class NewsModule {}
