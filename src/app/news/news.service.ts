import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Article } from './news';

import { Subject } from 'rxjs/Subject';

@Injectable()
export class NewsService {
    private news = new Subject();
    public readonly news$ = this.news.asObservable();
    constructor(private http: Http) {}

    getNews() {
        return this.http.get('https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty')
            .forEach(response => {
                const news = response.json();
                return news.map( (n: number) => {
                    this.getSingleNews(n).forEach(res => {
                        this.news.next(res.json());
                    })
                })
            })
    }
    getSingleNews(id: number) {
        return this.http.get(`https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`)
    }
}

