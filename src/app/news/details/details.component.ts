import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Article } from '../news';
import { NewsService } from '../news.service';

import 'rxjs/add/operator/map';

@Component({
  selector: 'news-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class NewsDetailsComponent implements OnInit, OnDestroy {
    id: number;
    article = {};
    private sub: any;
    constructor(private newsService: NewsService, private route: ActivatedRoute) {}
    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            this.id = +params['id'];
            // get single news return an http observable object
            this.newsService.getSingleNews(this.id).forEach( newsArticle => {
                this.article = newsArticle.json();
            })
        });
    }
    ngOnDestroy() {
        this.sub.unsubscribe();
    }
}
