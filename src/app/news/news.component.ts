import { Component } from '@angular/core';
import { NewsService } from './news.service';
import { Article } from './news';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})

export class NewsComponent {
    private news = [];

    constructor(private newsService: NewsService) {
        this.newsService.news$
            .subscribe(news => {
                this.news.push(news)
            });
        this.newsService.getNews();

    }
}
