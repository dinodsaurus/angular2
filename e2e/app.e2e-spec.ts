import { AllianzAngularPage } from './app.po';

describe('allianz-angular App', () => {
  let page: AllianzAngularPage;

  beforeEach(() => {
    page = new AllianzAngularPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
